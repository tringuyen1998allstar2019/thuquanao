class Product {
    constructor(id, type, name, desc, imgSrc_jpg, imgSrc_png) {
            this.id = id;
            this.type = type;
            this.name = name;
            this.desc = desc;
            this.imgSrc_jpg = imgSrc_jpg;
            this.imgSrc_png = imgSrc_png;
        }
        // "id": "topcloth_1",
        // "type": "topclothes",
        // "name": "Top Cloth 1",
        // "desc": "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
        // "imgSrc_jpg": "../assets/images/clothes/topcloth1_show.jpg",
        // "imgSrc_png": "../assets/images/clothes/topcloth1.png"
}
export default Product;